/*
 * Copyright 2018 TecAlliance GmbH.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.camunda.connect.ftp.impl;

import org.camunda.connect.ConnectorRequestException;
import org.camunda.connect.impl.ConnectLogger;

public class FtpConnectorLogger extends ConnectLogger {

    /**
     * Log that the connection has been established.
     *
     * @param url the host name of the connected FTP server
     * @param port the port number
     */
    public void connected(String url, int port) {
        logDebug("001", "Connected to FTP server at {}:{}", url, port);
    }

    /**
     * Log that the login to the FTP server has succeeded.
     *
     * @param username the username used for login
     */
    public void loggedIn(String username) {
        logDebug("002", "Logged-in to FTP server with username '{}'", username);
    }

    /**
     * If disconnecting fails, we might continue safely but we should output a
     * warning message.
     *
     * @param cause the root cause
     */
    public void disconnectFailed(Exception cause) {
        logWarn("003", "Failed to disconnect from FTP server.", cause);
    }

    /**
     * Use in case the FTP client cannot connect for whatever reason.
     *
     * @param cause the root cause
     * @return the wrapped exception
     */
    public ConnectorRequestException unableToConnect(Exception cause) {
        return new ConnectorRequestException(exceptionMessage("004", "Unable to connect to FTP server", cause));
    }

    /**
     * Use in case the FTP client cannot login for whatever reason.
     *
     * @param cause the root cause
     * @return the wrapped exception
     */
    public ConnectorRequestException unableToLogin(Exception cause) {
        return new ConnectorRequestException(exceptionMessage("005", "Unable to login to FTP server - check username/password", cause));
    }

    /**
     * Use in case the FTP client cannot execute the desired command.
     *
     * @param cause the root cause
     * @return the wrapped exception
     */
    public ConnectorRequestException unableToExecuteRequest(Exception cause) {
        return new ConnectorRequestException(exceptionMessage("006", "Unable to execute FTP command", cause));
    }

    /**
     * Use in case the login failed with the given username/password
     * combination.
     *
     * @param username the username used for login
     * @param password the password used for login
     * @return the exception
     */
    public ConnectorRequestException loginFailed(String username, String password) {
        String maskedPassword = password.replaceAll(".", "*");
        return new ConnectorRequestException(exceptionMessage("007", "Cannot login to FTP server using username '{}' and password '{}'", username, maskedPassword));
    }

    /**
     * Use if a parameter is missing.
     *
     * @param command the FTP command which needs the parameter
     * @param parameter the name of the missing parameter
     * @return the exception
     */
    public ConnectorRequestException parameterMissing(String command, String parameter) {
        return new ConnectorRequestException(exceptionMessage("008", "Please specify the '{}' parameter! Cannot execute command '{}' without it.", parameter, command));
    }

    /**
     * Use if a given file is not readable, does not exist etc.
     *
     * @return the exception
     */
    public ConnectorRequestException fileError() {
        return new ConnectorRequestException(exceptionMessage("009", "Local file unreadable or already at the end of the stream"));
    }

    /**
     * Use if the configured FTP command is not supported by the connector.
     *
     * @param command the FTP command
     * @return the exception
     */
    public ConnectorRequestException unsupportedCommand(String command) {
        return new ConnectorRequestException(exceptionMessage("010", "FTP command '{}' is not supported by this connector!", command));
    }

}
