/*
 * Copyright 2018 TecAlliance GmbH.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.camunda.connect.ftp.impl;

import java.io.IOException;
import java.io.InputStream;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPCmd;
import org.camunda.connect.ftp.BaseFtpConnectorRequest;
import org.camunda.connect.ftp.FtpConnectorResponse;
import org.camunda.connect.impl.AbstractConnector;

public abstract class AbstractFtpConnectorImpl<Q extends BaseFtpConnectorRequest<Q, R>, R extends FtpConnectorResponse>
        extends AbstractConnector<Q, R> {

    protected static FtpConnectorLogger LOG = FtpLogger.FTP_LOGGER;

    protected FTPClient ftpClient;

    public AbstractFtpConnectorImpl(String connectorId) {
        super(connectorId);
        this.ftpClient = new FTPClient();
    }

    @Override
    public R execute(Q request) {
        connect(request);
        login(request);
        FtpConnectorRequestInvocation invocation = createInvocation(request);

        try {
            R response = createResponse((FtpCommandResult) invocation.proceed());
            ftpClient.disconnect();
            return response;
        } catch (Exception ex) {
            throw LOG.unableToExecuteRequest(ex);
        }
    }

    protected abstract R createResponse(FtpCommandResult result);

    protected void connect(Q request) {
        String url = request.getUrl();
        int port = request.getPort();
        try {
            ftpClient.connect(url, port);
        } catch (IOException ex) {
            throw LOG.unableToConnect(ex);
        }
        LOG.connected(url, port);
    }

    protected void disconnect() {
        try {
            ftpClient.disconnect();
        } catch (IOException ex) {
            LOG.disconnectFailed(ex);
        }
    }

    protected void login(Q request) {
        String username = request.getUsername();
        String password = request.getPassword();
        try {
            boolean loggedIn = ftpClient.login(username, password);
            if (loggedIn) {
                LOG.loggedIn(username);
            } else {
                throw LOG.loginFailed(username, password);
            }
        } catch (IOException ex) {
            throw LOG.unableToLogin(ex);
        }
    }

    protected FtpConnectorRequestInvocation createInvocation(Q request) {
        String command = request.getCommand();
        FtpConnectorRequestInvocation invocation;

        if (FTPCmd.LIST.getCommand().equalsIgnoreCase(command)) {
            invocation = new FtpConnectorRequestInvocation(FTPCmd.LIST, request, requestInterceptors, ftpClient);
            invocation.setRemoteName(request.getRemoteName());
            invocation.setFileFilter(request.getFilefilter());
            return invocation;
        } else if (FTPCmd.RETR.getCommand().equalsIgnoreCase(command)) {
            String fileName = request.getRemoteName();
            checkRemoteName(fileName, command);
            invocation = new FtpConnectorRequestInvocation(FTPCmd.RETR, request, requestInterceptors, ftpClient);
            invocation.setRemoteName(fileName);
            return invocation;
        } else if (FTPCmd.STOR.getCommand().equalsIgnoreCase(command)) {
            InputStream localFile = request.getLocalFile();
            String remoteName = request.getRemoteName();
            try {
                if (localFile == null) {
                    throw LOG.parameterMissing(command, Q.PARAM_NAME_REQUEST_LOCAL_FILE);
                } else if (localFile.available() <= 0) {
                    throw LOG.fileError();
                }
            } catch (IOException ex) {
                throw LOG.unableToExecuteRequest(ex);
            }
            checkRemoteName(remoteName, command);
            invocation = new FtpConnectorRequestInvocation(FTPCmd.STOR, request, requestInterceptors, ftpClient);
            invocation.setRemoteName(remoteName);
            invocation.setLocalFileStream(localFile);
            return invocation;
        } else if (FTPCmd.DELE.getCommand().equalsIgnoreCase(command)) {
            String fileName = request.getRemoteName();
            checkRemoteName(fileName, command);
            invocation = new FtpConnectorRequestInvocation(FTPCmd.DELE, request, requestInterceptors, ftpClient);
            invocation.setRemoteName(fileName);
            return invocation;
        } else {
            throw LOG.unsupportedCommand(command);
        }
    }

    private void checkRemoteName(String remoteName, String command) {
        if (remoteName == null || remoteName.isEmpty()) {
            throw LOG.parameterMissing(command, Q.PARAM_NAME_REQUEST_REMOTE_NAME);
        }
    }

    /**
     * @return the Apache FTP Client instance
     */
    public FTPClient getFtpClient() {
        return ftpClient;
    }

    /**
     * Allows to override the Apache FTP client to be used.
     *
     * @param ftpClient the Apache FTP client
     */
    public void setFtpClient(FTPClient ftpClient) {
        this.ftpClient = ftpClient;
    }

}
