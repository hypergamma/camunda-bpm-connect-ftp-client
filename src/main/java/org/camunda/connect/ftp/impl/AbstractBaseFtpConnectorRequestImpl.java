/*
 * Copyright 2018 TecAlliance GmbH.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.camunda.connect.ftp.impl;

import java.io.InputStream;
import org.camunda.connect.ftp.BaseFtpConnectorRequest;
import org.camunda.connect.ftp.FtpConnectorResponse;
import org.camunda.connect.impl.AbstractConnectorRequest;
import org.camunda.connect.spi.Connector;

public abstract class AbstractBaseFtpConnectorRequestImpl<Q extends BaseFtpConnectorRequest<?, ?>, R extends FtpConnectorResponse>
        extends AbstractConnectorRequest<R>
        implements BaseFtpConnectorRequest<Q, R> {

    public AbstractBaseFtpConnectorRequestImpl(Connector connector) {
        super(connector);
    }

    @Override
    public Q url(String url) {
        setRequestParameter(BaseFtpConnectorRequest.PARAM_NAME_REQUEST_URL, url);
        return (Q) this;
    }

    @Override
    public Q port(int port) {
        setRequestParameter(BaseFtpConnectorRequest.PARAM_NAME_REQUEST_PORT, "" + port);
        return (Q) this;
    }

    @Override
    public Q remoteName(String remoteName) {
        setRequestParameter(BaseFtpConnectorRequest.PARAM_NAME_REQUEST_REMOTE_NAME, remoteName);
        return (Q) this;
    }

    @Override
    public Q filefilter(String filefilter) {
        setRequestParameter(BaseFtpConnectorRequest.PARAM_NAME_REQUEST_FILEFILTER, filefilter);
        return (Q) this;
    }

    @Override
    public Q localFile(InputStream localFile) {
        setRequestParameter(BaseFtpConnectorRequest.PARAM_NAME_REQUEST_LOCAL_FILE, localFile);
        return (Q) this;
    }

    @Override
    public Q username(String username) {
        setRequestParameter(BaseFtpConnectorRequest.PARAM_NAME_REQUEST_USERNAME, username);
        return (Q) this;
    }

    @Override
    public Q password(String password) {
        setRequestParameter(BaseFtpConnectorRequest.PARAM_NAME_REQUEST_PASSWORD, password);
        return (Q) this;
    }

    @Override
    public Q command(String command) {
        setRequestParameter(BaseFtpConnectorRequest.PARAM_NAME_REQUEST_COMMAND, command);
        return (Q) this;
    }

    @Override
    public String getUrl() {
        return (String) getRequestParameter(BaseFtpConnectorRequest.PARAM_NAME_REQUEST_URL);
    }

    @Override
    public int getPort() {
        String port = (String) getRequestParameter(BaseFtpConnectorRequest.PARAM_NAME_REQUEST_PORT);
        if (port == null) {
            port = "21";
        }
        return Integer.parseInt(port);
    }

    @Override
    public String getRemoteName() {
        return (String) getRequestParameter(BaseFtpConnectorRequest.PARAM_NAME_REQUEST_REMOTE_NAME);
    }

    @Override
    public String getFilefilter() {
        return (String) getRequestParameter(BaseFtpConnectorRequest.PARAM_NAME_REQUEST_FILEFILTER);
    }

    @Override
    public InputStream getLocalFile() {
        return (InputStream) getRequestParameter(BaseFtpConnectorRequest.PARAM_NAME_REQUEST_LOCAL_FILE);
    }

    @Override
    public String getUsername() {
        return (String) getRequestParameter(BaseFtpConnectorRequest.PARAM_NAME_REQUEST_USERNAME);
    }

    @Override
    public String getPassword() {
        return (String) getRequestParameter(BaseFtpConnectorRequest.PARAM_NAME_REQUEST_PASSWORD);
    }

    @Override
    public String getCommand() {
        return (String) getRequestParameter(BaseFtpConnectorRequest.PARAM_NAME_REQUEST_COMMAND);
    }

}
