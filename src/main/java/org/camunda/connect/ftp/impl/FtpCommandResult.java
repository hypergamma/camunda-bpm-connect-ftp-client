/*
 * Copyright 2018 TecAlliance GmbH.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.camunda.connect.ftp.impl;

import java.io.File;
import java.util.Collections;
import java.util.List;

public class FtpCommandResult {

    private boolean commandSuccess;

    private File retrievedFile;

    private List<String> fileNames;

    public FtpCommandResult(boolean commandSuccess) {
        this(commandSuccess, null, null);
    }

    public FtpCommandResult(boolean commandSuccess, File retrievedFile) {
        this(commandSuccess, retrievedFile, null);
    }

    public FtpCommandResult(boolean commandSuccess, List<String> fileNames) {
        this(commandSuccess, null, fileNames);
    }

    private FtpCommandResult(boolean commandSuccess, File retrievedFile, List<String> fileNames) {
        this.commandSuccess = commandSuccess;
        this.retrievedFile = retrievedFile;
        this.fileNames = fileNames;
    }

    /**
     * @return true if the FTP command completed successfully
     */
    public boolean isCommandSuccess() {
        return commandSuccess;
    }

    /**
     * @param commandSuccess true if the FTP command completed successfully
     */
    public void setCommandSuccess(boolean commandSuccess) {
        this.commandSuccess = commandSuccess;
    }

    /**
     * @return the retrieved file or {@code null} e. g. if no RETR was executed
     */
    public File getRetrievedFile() {
        return retrievedFile;
    }

    /**
     * @param retrievedFile the retrieved file in case RETR was executed
     */
    public void setRetrievedFile(File retrievedFile) {
        this.retrievedFile = retrievedFile;
    }

    /**
     * @return the list of files as a result of the LIST command
     */
    public List<String> getFileNames() {
        if (fileNames == null) {
            return null;
        } else {
            return Collections.unmodifiableList(fileNames);
        }
    }

    /**
     * @param fileNames the list of file if LIST was executed
     */
    public void setFileNames(List<String> fileNames) {
        if (fileNames == null) {
            this.fileNames = null;
        } else {
            this.fileNames = Collections.unmodifiableList(fileNames);
        }
    }

}
