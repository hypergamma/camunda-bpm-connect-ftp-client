/*
 * Copyright 2018 TecAlliance GmbH.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.camunda.connect.ftp.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPCmd;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPFileFilter;
import org.camunda.connect.impl.AbstractRequestInvocation;
import org.camunda.connect.spi.ConnectorRequest;
import org.camunda.connect.spi.ConnectorRequestInterceptor;

public class FtpConnectorRequestInvocation extends AbstractRequestInvocation<FTPCmd> {

    private final FTPClient ftpClient;
    private String remoteName;
    private InputStream localFileStream;
    private String fileFilter;
    static final FtpConnectorLogger LOG = FtpLogger.FTP_LOGGER;

    public FtpConnectorRequestInvocation(
            FTPCmd target,
            ConnectorRequest<?> request,
            List<ConnectorRequestInterceptor> interceptorChain,
            FTPClient ftpClient) {

        super(target, request, interceptorChain);
        this.ftpClient = ftpClient;

    }

    @Override
    public Object invokeTarget() throws Exception {
        switch (target) {
            case LIST:
                return list();
            case RETR:
                return retr();
            case STOR:
                return stor();
            case DELE:
                return dele();
            default:
                // should never happen, b/c it's already checked
                return null;
        }
    }

    private FtpCommandResult list() {
        final String pattern;
        if (fileFilter == null || fileFilter.isEmpty()) {
            pattern = ".*";
        } else {
            pattern = fileFilter;
        }

        try {
            // Get the file listing from the FTP server
            FTPFile[] files = ftpClient.listFiles(remoteName, new FTPFileFilter() {
                @Override
                public boolean accept(FTPFile file) {
                    return file.getName().matches(pattern);
                }
            });
            // Read the file names and populate the result object
            List<String> fileNames = new ArrayList<>();
            for (FTPFile ff : files) {
                fileNames.add(ff.getName());
            }
            return new FtpCommandResult(true, fileNames);
        } catch (IOException ex) {
            throw LOG.unableToExecuteRequest(ex);
        }
    }

    private FtpCommandResult retr() {
        try {
            File tempFile = File.createTempFile("camunda-ftp-connector-" + remoteName, null);
            FileOutputStream outputStream = new FileOutputStream(tempFile);
            // download the desired file, then close the stream
            boolean success = ftpClient.retrieveFile(remoteName, outputStream);
            outputStream.close();
            return new FtpCommandResult(success, tempFile);
        } catch (IOException ex) {
            throw LOG.unableToExecuteRequest(ex);
        }
    }

    private FtpCommandResult stor() {
        try {
            // upload the file, then close the local InputStream
            boolean success = ftpClient.storeFile(remoteName, localFileStream);
            localFileStream.close();
            return new FtpCommandResult(success);
        } catch (IOException ex) {
            throw LOG.unableToExecuteRequest(ex);
        }
    }

    private FtpCommandResult dele() {
        try {
            // actually delete the file
            boolean success = ftpClient.deleteFile(remoteName);
            return new FtpCommandResult(success);
        } catch (IOException ex) {
            throw LOG.unableToExecuteRequest(ex);
        }
    }

    /**
     * @return the remote file or directory name
     */
    public String getRemoteName() {
        return remoteName;
    }

    /**
     * Set the remote file or directory name for the LIST, RETR or DELE command.
     *
     * @param remoteName a remote file or directory name
     */
    public void setRemoteName(String remoteName) {
        this.remoteName = remoteName;
    }

    /**
     * @return the InputStream of the local file
     */
    public InputStream getLocalFileStream() {
        return localFileStream;
    }

    /**
     * @param localFileStream a local file to upload (STOR)
     */
    public void setLocalFileStream(InputStream localFileStream) {
        this.localFileStream = localFileStream;
    }

    /**
     * @return the filename filter pattern (regular expression)
     */
    public String getFileFilter() {
        return fileFilter;
    }

    /**
     * @param fileFilter the filename filter pattern (regular expression)
     */
    public void setFileFilter(String fileFilter) {
        this.fileFilter = fileFilter;
    }

}
