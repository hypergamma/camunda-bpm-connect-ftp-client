/*
 * Copyright 2018 TecAlliance GmbH.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this localFile except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.camunda.connect.ftp.impl;

import org.apache.commons.net.ftp.FTPCmd;
import org.camunda.connect.ftp.FtpConnectorRequest;
import org.camunda.connect.ftp.FtpConnectorResponse;
import org.camunda.connect.spi.Connector;

public class FtpConnectorRequestImpl
        extends AbstractBaseFtpConnectorRequestImpl<FtpConnectorRequest, FtpConnectorResponse>
        implements FtpConnectorRequest {

    public FtpConnectorRequestImpl(Connector connector) {
        super(connector);
    }

    @Override
    public FtpConnectorRequest list() {
        return command(FTPCmd.LIST.getCommand());
    }

    @Override
    public FtpConnectorRequest retr() {
        return command(FTPCmd.RETR.getCommand());
    }

    @Override
    public FtpConnectorRequest dele() {
        return command(FTPCmd.DELE.getCommand());
    }

    @Override
    public FtpConnectorRequest stor() {
        return command(FTPCmd.STOR.getCommand());
    }

}
