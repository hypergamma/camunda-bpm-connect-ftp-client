/*
 * Copyright 2018 TecAlliance GmbH.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.camunda.connect.ftp.impl;

import java.util.Map;
import org.camunda.connect.ftp.FtpConnectorResponse;
import org.camunda.connect.impl.AbstractConnectorResponse;

public class FtpConnectorResponseImpl extends AbstractConnectorResponse implements FtpConnectorResponse {

    private final FtpCommandResult ftpResult;
    
    public FtpConnectorResponseImpl(FtpCommandResult ftpResult) {
        this.ftpResult = ftpResult;
    }
    
    @Override
    protected void collectResponseParameters(Map<String, Object> responseParameters) {
        responseParameters.put(PARAM_NAME_FILE, ftpResult.getRetrievedFile());
        responseParameters.put(PARAM_NAME_FILE_NAMES, ftpResult.getFileNames());
        responseParameters.put(PARAM_NAME_FTP_SUCCESS, ftpResult.isCommandSuccess());
    }

}
