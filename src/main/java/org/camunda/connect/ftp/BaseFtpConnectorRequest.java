/*
 * Copyright 2018 TecAlliance GmbH.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.camunda.connect.ftp;

import java.io.InputStream;
import org.camunda.connect.spi.ConnectorRequest;
import org.camunda.connect.spi.ConnectorResponse;

public interface BaseFtpConnectorRequest<Q extends BaseFtpConnectorRequest<?, ?>, R extends ConnectorResponse> extends ConnectorRequest<R> {

    public static final String PARAM_NAME_REQUEST_URL = "url";
    public static final String PARAM_NAME_REQUEST_PORT = "port";
    public static final String PARAM_NAME_REQUEST_COMMAND = "command";
    public static final String PARAM_NAME_REQUEST_REMOTE_NAME = "remotename";
    public static final String PARAM_NAME_REQUEST_FILEFILTER = "filefilter";
    public static final String PARAM_NAME_REQUEST_LOCAL_FILE = "localfile";
    public static final String PARAM_NAME_REQUEST_USERNAME = "username";
    public static final String PARAM_NAME_REQUEST_PASSWORD = "password";

    /**
     * Sets the URL of the FTP server to use. Examples:
     * <ul>
     * <li>192.168.178.12</li>
     * <li>localhost</li>
     * <li>ftp.mycompany.com</li>
     * </ul>
     *
     * @param url the URL to use
     * @return the request object for further invocations
     */
    Q url(String url);

    /**
     * @return the URL configured for this request.
     */
    String getUrl();

    /**
     * Sets the port number of the FTP server to use.
     *
     * @param port the port number
     * @return the request object for further invocations
     */
    Q port(int port);

    /**
     * @return the port number configured for this request
     */
    int getPort();

    /**
     * Sets the username for the login to the FTP server.
     *
     * @param username the username
     * @return the request object for further invocations
     */
    Q username(String username);

    /**
     * @return the username configured for login
     */
    String getUsername();

    /**
     * Sets the password to use during login to the FTP server.
     *
     * @param password the password
     * @return the request object for further invocations
     */
    Q password(String password);

    /**
     * @return the password configured for login
     */
    String getPassword();

    /**
     * Sets the remote file/directory remoteName for the {@code LIST}
     * (optional), {@code STOR}, {@code RETR}, {@code DELE} operations.
     *
     * @param remoteName the remoteName of the file or directory
     * @return the request object for further invocations
     */
    Q remoteName(String remoteName);

    /**
     * @return the remote file/directory remoteName configured for this request
     */
    String getRemoteName();

    /**
     * Sets an optional filter for file names. The filter supports regular
     * expressions by calling the
     * {@link String#matches(java.lang.String) matches()} method.
     * <p>
     * <b>Example:</b><br>
     * <code>
     * "29111-wow.xml".matches("[0-9]{5}-[A-z]{1,3}\\.xml") // true<br>
     * "hello.txt".matches("hello.json") // false<br>
     * "hello.txt".matches("hello") // false<br>
     * "hello.txt".matches("hello.*") // true
     * </code>
     *
     * @param filefilter the filter
     * @return the request object for further invocations
     */
    Q filefilter(String filefilter);

    /**
     * @return the file name filter configured for this request
     */
    String getFilefilter();

    /**
     * Sets a locally available local file for the {@code STOR} operation.
     *
     * @param localFile the local file to upload to the FTP server
     * @return the request object for further invocations
     */
    Q localFile(InputStream localFile);

    /**
     * @return the local local file configured for this request
     */
    InputStream getLocalFile();

    /**
     * Sets the command to send to the FTP server.
     *
     * @param command the command
     * @return the request object for further invocations
     */
    Q command(String command);

    /**
     * @return the command configured for this request
     */
    String getCommand();
}
