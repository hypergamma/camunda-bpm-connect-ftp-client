/*
 * Copyright 2018 TecAlliance GmbH.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.camunda.connect.ftp;

public interface FtpCommandConnectorRequest<Q extends BaseFtpConnectorRequest<?, ?>, R extends FtpConnectorResponse> extends BaseFtpConnectorRequest<Q, R> {

    /**
     * Sets this request as a {@code LIST} command. The connector will then list
     * the <strong>files</strong> contained in a directory which is either
     * specified by {@link BaseFtpConnectorRequest#name(java.lang.String) name}
     * or just in the root directory /.
     *
     * @return the request object for further invocations
     */
    Q list();

    /**
     * Sets this request as a {@code RETR} command. The connector will then
     * retrieve a file specified by
     * {@link BaseFtpConnectorRequest#name(java.lang.String) name}. If the name
     * was not given, nothing may happen.
     *
     * @return the request object for further invocations
     */
    Q retr();

    /**
     * Sets this request as a {@code DELE} command. The connector will then try
     * to delete the file specified by
     * {@link BaseFtpConnectorRequest#name(java.lang.String) name}. If name was
     * not given, nothing may happen.
     *
     * @return the request object for further invocations
     */
    Q dele();

    /**
     * Sets this request as a {@code STOR} command. The connector will then try
     * to upload the file specified by
     * {@link BaseFtpConnectorRequest#file(java.io.File) file}. If file was not
     * given, nothing may happen.
     *
     * @return the request object for further invocations
     */
    Q stor();

}
