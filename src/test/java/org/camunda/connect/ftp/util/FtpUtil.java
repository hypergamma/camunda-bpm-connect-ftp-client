/*
 * Copyright 2018 TecAlliance GmbH.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.camunda.connect.ftp.util;

import java.util.Arrays;
import org.apache.ftpserver.FtpServer;
import org.apache.ftpserver.FtpServerFactory;
import org.apache.ftpserver.ftplet.Authority;
import org.apache.ftpserver.ftplet.UserManager;
import org.apache.ftpserver.listener.ListenerFactory;
import org.apache.ftpserver.usermanager.ClearTextPasswordEncryptor;
import org.apache.ftpserver.usermanager.PropertiesUserManagerFactory;
import org.apache.ftpserver.usermanager.impl.BaseUser;
import org.apache.ftpserver.usermanager.impl.WritePermission;

public class FtpUtil {

    private static FtpServer ftpServer;

    public static FtpServer startFtp() throws Exception {
        FtpServerFactory serverFactory = new FtpServerFactory();
        // Let FTP server listen on port to 7021
        ListenerFactory listenerFactory = new ListenerFactory();
        listenerFactory.setPort(7021);
        serverFactory.addListener("default", listenerFactory.createListener());
        // Add user for login
        PropertiesUserManagerFactory userManagerFactory = new PropertiesUserManagerFactory();
        userManagerFactory.setUrl(FtpUtil.class.getResource("/testusers.properties"));
        userManagerFactory.setPasswordEncryptor(new ClearTextPasswordEncryptor());
        BaseUser user = new BaseUser();
        user.setName("admin");
        user.setPassword("secret");
        user.setHomeDirectory(FtpUtil.class.getResource("/ftp-home").getPath());
        user.setAuthorities(Arrays.asList(new Authority[]{new WritePermission()}));
        UserManager userManager = userManagerFactory.createUserManager();
        userManager.save(user);
        serverFactory.setUserManager(userManager);
        // Finally start the FTP server
        ftpServer = serverFactory.createServer();
        ftpServer.start();
        return ftpServer;
    }

    public static void stopFtp() throws Exception {
        ftpServer.stop();
    }

}
