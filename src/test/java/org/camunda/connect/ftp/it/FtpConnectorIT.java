/*
 * Copyright 2018 TecAlliance GmbH.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.camunda.connect.ftp.it;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.camunda.bpm.engine.test.Deployment;
import org.camunda.bpm.engine.test.ProcessEngineRule;
import static org.camunda.bpm.engine.test.assertions.ProcessEngineAssertions.assertThat;
import static org.camunda.bpm.engine.test.assertions.ProcessEngineAssertions.init;
import static org.camunda.bpm.engine.test.assertions.ProcessEngineTests.runtimeService;
import org.camunda.connect.ftp.util.FtpUtil;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;

@Deployment(resources = {
    "bpmn/FtpConnectorIT.testList.bpmn20.xml",
    "bpmn/FtpConnectorIT.testRetr.bpmn20.xml",
    "bpmn/FtpConnectorIT.testStor.bpmn20.xml",
    "bpmn/FtpConnectorIT.testDele.bpmn20.xml"
})
public class FtpConnectorIT {

    @Rule
    public ProcessEngineRule rule = new ProcessEngineRule();

    @BeforeClass
    public static void setUpClass() throws Exception {
        FtpUtil.startFtp();
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
        FtpUtil.stopFtp();
    }

    @Before
    public void setUp() throws Exception {
        init(rule.getProcessEngine());
    }

    @Test
    public void testList() {
        ProcessInstance instance = runtimeService()
                .startProcessInstanceByKey("LIST");
        // Verify that process has executed the FTP connector successfully and
        // that it returned a testList of files
        assertThat(instance)
                .isStarted()
                .hasPassed("list-files-on-ftp")
                .hasVariables("filenames", "ftpsuccess");
        assertThat(instance)
                .variables()
                .containsEntry("ftpsuccess", Boolean.TRUE);
        // Verify that the testList of files contains the two expected files
        List<String> filenames = (List<String>) runtimeService()
                .getVariable(instance.getProcessInstanceId(), "filenames");
        assertTrue("Expected a file list to be retrieved from FTP",
                filenames.contains("2018-09-28_10_25_45_TecRMI_1810_de-DE_es-ES.xml"));
        assertTrue("Expected a file list to be retrieved from FTP",
                filenames.contains("2018-09-28_10_26_09_TecRMI_1810_de-DE_sk-SK.xml"));
    }

    @Test
    public void testRetr() throws Exception {
        ProcessInstance instance = runtimeService()
                .startProcessInstanceByKey("RETR");
        // Verify that the process has executed the connector
        assertThat(instance)
                .isStarted()
                .hasPassed("retr-file-from-ftp")
                .hasVariables("retrievedFile");
        // Verify the download by comparing the contents of the retrieved file
        File retrievedFile = (File) runtimeService()
                .getVariable(instance.getProcessInstanceId(), "retrievedFile");
        String content = readStreamFully(new FileInputStream(retrievedFile));
        assertTrue("Expected content to start with Placeat dolorem...",
                content.startsWith("Placeat dolorem ipsam dolorem quibusdam"));
    }

    @Test
    public void testStor() throws Exception {
        ProcessInstance instance = runtimeService()
                .startProcessInstanceByKey("STOR");
        // Verify that the process has executed the connector
        assertThat(instance)
                .isStarted()
                .hasPassed("stor-file-on-ftp")
                .hasVariables("ftpsuccess");
        assertThat(instance)
                .variables()
                .containsEntry("ftpsuccess", Boolean.TRUE);
        // Verify that the desired file has been uploaded to the FTP server by
        // comparing contents of the file in the FTP home directory. Please note
        // that the file is being created in the process definition!
        File uploadedFile = new File(FtpConnectorIT.class.getResource("/ftp-home/FtpTest.testStor.txt").toURI());
        String content = readStreamFully(new FileInputStream(uploadedFile));
        String expectedContent = "Hello from FtpConnectorIT.testStor.bpmn20.xml";
        assertEquals("Expected file to be uploaded completely to the FTP server", expectedContent, content);
        // Clean up (delete the uploaded file)
        uploadedFile.delete();
    }

    @Test
    public void testDele() throws Exception {
        // Prerequisite: Add file to the FTP home directory
        File deletedFile = new File(
                FtpConnectorIT.class.getResource("/ftp-home").getPath()
                + File.separator
                + "FtpTest.testDele.txt");
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(deletedFile))) {
            writer.append("Hello from FtpConnectorIT.testDele!");
        }
        assertTrue("Failed to set up test: Could not create the file we want to delete during the test", deletedFile.exists());
        // Start the process
        ProcessInstance instance = runtimeService()
                .startProcessInstanceByKey("DELE");
        // Verify that the process has executed the connector
        assertThat(instance)
                .isStarted()
                .hasPassed("dele-file-from-ftp")
                .hasVariables("ftpsuccess");
        assertThat(instance)
                .variables()
                .containsEntry("ftpsuccess", Boolean.TRUE);
        assertFalse("Expected file to be deleted", deletedFile.exists());
    }

    private String readStreamFully(InputStream stream) throws Exception {
        StringBuilder contentBuilder = new StringBuilder();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(stream))) {
            while (reader.ready()) {
                contentBuilder.append(reader.readLine());
            }
        }
        return contentBuilder.toString();
    }

}
